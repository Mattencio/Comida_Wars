﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverCamara : MonoBehaviour {

	//necesita muchas modificaciones aún

    public float speed = 0.1f;
    private float gradosFoV;
    private float gradosComplementario;
    private float dx;
    private float dy;
    private float anguloZoom = 50f;

    private float distanciaPrevia;
    public float zoomVel = 1.0f;

	float radAngle;
	float positionXclamp;
	float radHFOV;
	float positionYclamp;

	// Update is called once per frame
	void Update () {

	
		ajustarPantalla();

		if ( (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved) && Jugador.estaManipulando == false)
        {
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            transform.Translate(-touchDeltaPosition.x * speed * Time.deltaTime, -touchDeltaPosition.y * speed * Time.deltaTime, 0);

			ajustarPantalla();
        }

        if(Input.touchCount == 2 && 
        (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(1).phase == TouchPhase.Began)) 
        {
			anguloZoom = Camera.main.fieldOfView;
        	distanciaPrevia = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
        }
        else if (Input.touchCount == 2 &&
        (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved) ) 
        {
        	float distancia;
        	Vector2 touch1 = Input.GetTouch(0).position;
        	Vector2 touch2 = Input.GetTouch(1).position;

        	distancia = Vector2.Distance(touch1, touch2);

        	float zoomGanancia = distancia - distanciaPrevia;
        	zoomGanancia = - zoomGanancia / 100f;

			Camera.main.fieldOfView = Mathf.Clamp(anguloZoom + zoomGanancia * zoomVel, 20f, 85f);

			ajustarPantalla();
        }
        

	}

	private void ajustarPantalla ()
	{
		radAngle = Camera.main.fieldOfView * Mathf.Deg2Rad;
		radHFOV = 2 * Mathf.Atan(Mathf.Tan(radAngle / 2) * Camera.main.aspect);
		gradosFoV = Mathf.Rad2Deg * radHFOV;
		gradosComplementario = 90f - ( gradosFoV / 2 );
		gradosComplementario = gradosComplementario * Mathf.PI / 180;

        dx = 10 / Mathf.Tan(gradosComplementario);

		gradosFoV = Camera.main.fieldOfView;
		gradosComplementario = 90f - ( gradosFoV / 2 );
		gradosComplementario = gradosComplementario * Mathf.PI / 180;

        dy = 10 / Mathf.Tan(gradosComplementario);			

		positionXclamp = Mathf.Clamp(transform.position.x - dx, -8.4f, 24.3f - 2 * dx);
		positionYclamp = Mathf.Clamp(transform.position.y - dy, -5f, 1.4f);

		transform.position = new Vector3 (positionXclamp + dx, positionYclamp + dy, transform.position.z);
	}
}
