﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayersManager : NetworkBehaviour {

	// Use this for initialization
	public static PlayersManager PM;
	public static string textoPlayer = "Versión alpha de comida wars";

    public SyncListInt arrayIDServer = new SyncListInt();

    public SyncListString arrayIPServer = new SyncListString();

    [SyncVar]
    public int playerIDManager = 0;

    void Awake()
    {
	if(PM != null)
		GameObject.Destroy(PM);
        else
		PM = this;
         
        DontDestroyOnLoad(this);
    }

    public static void incrementarID() {

        PM.playerIDManager++;
    }

    public static int recibirID()
    {
        return PM.playerIDManager;
    }

    public static void arrayIPsave(string _ip, int _id) {
        PM.arrayIPServer.Add(_ip);
        PM.arrayIDServer.Add(_id);
    }

    public static SyncListString getArrayIPserver()
    {
        return PM.arrayIPServer;
    }

}
