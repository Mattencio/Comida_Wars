﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Networking;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    // #Aldo implementación del uimanager

    public int currentState = 0;
    public int nextState = 0;
    public static UIManager singleton = null;

    const int MAIN_MENU = 0;
    const int GAMEPLAY = 1;
    const int GAME_OVER = 2;
    const int ABANDONAR = 3;

    Transform mainMenuPanel;
    Transform gameplayHUD;
    Transform abandonarPanel;
    Transform gameOverGUI;

    public string ipAddress;

    private void Awake()
    {
        if (singleton == null)
        {
            singleton = this;
        }
        else if (singleton != this)
        {
            Destroy(gameObject);
        }

        //DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        // Obtener elementos de cada estado
        mainMenuPanel = transform.Find("Main Menu");
        gameplayHUD = transform.Find("Gameplay");
        abandonarPanel = transform.Find("Abandonar partida");
        gameOverGUI = transform.Find("Game Over");
        currentState = -1;
    }

    // Update is called once per frame
    void LateUpdate () {
        // Actualizar el estado actual y llamar a la función que lo inicialize
        if (currentState != nextState)
        {
            switch(nextState)
            {
                case MAIN_MENU:
                    MostrarMainMenu();
                    break;
                case GAMEPLAY:
                    MostrarGameplay();
                    break;
                case GAME_OVER:
                    MostrarGameOver();
                    break;
                case ABANDONAR:
                    MostrarAbandonar();
                    break;
            }

            currentState = nextState;
        }
	}

    public void switchState(int state)
    {
        // Definir el siguiente estado a usar en el próximo update
        nextState = state;
    }

    void MostrarMainMenu()
    {
        // Dependiendo del estado actual se van a desactivar el gui actual
        switch (currentState)
        {
            case GAME_OVER:
                gameOverGUI.gameObject.SetActive(false);
                break;
            case ABANDONAR:
                abandonarPanel.gameObject.SetActive(false);
                break;
        }

        // Activar el gui a mostrar e inicializar
        mainMenuPanel.gameObject.SetActive(true);

        Transform objetoCrearPartida = mainMenuPanel.Find("Crear partida");
        if (objetoCrearPartida == null)
        {
            print("Error Crear partida no encontrado");
            return;
        }
        Button botonCrearPartida = objetoCrearPartida.GetComponent<Button>();
        botonCrearPartida.onClick.RemoveAllListeners();
        botonCrearPartida.onClick.AddListener(CrearPartida);

        Transform objetoUnirsePartida = mainMenuPanel.Find("Unirse a partida");
        if (objetoUnirsePartida == null)
        {
            print("Error Unirse a partida no encontrado");
            return;
        }
        Button botonUnirsePartida = objetoUnirsePartida.GetComponent<Button>();
        botonUnirsePartida.onClick.RemoveAllListeners();
        botonUnirsePartida.onClick.AddListener(UnirConIP);
    }

    void CrearPartida()
    {
        // función para crear partida y cambiar el estado a gameplay
        NetworkManager_Custom.StartupHost();
        switchState(GAMEPLAY);
    }

    void UnirConIP()
    {
        // función para obtener IP y empezar corutina para esperar conexión
        UIManager.singleton.ipAddress = mainMenuPanel.Find("Server IP").Find("Text").GetComponent<Text>().text;

        StartCoroutine(esperandoConexion());
        
    }

    IEnumerator esperandoConexion()
    {
        // función para esperar a que la conexión se establezca, se puede agregar un loading con ayuda de esto
        float timeout = Time.time + 3.0f;
        NetworkClient newClient = NetworkManager_Custom.JoinGame(ipAddress);
        while (!newClient.isConnected && Time.time < timeout)
        {
            print("esperando");
            yield return new WaitForSeconds(0.5f);
        }
        if (newClient.isConnected)
        {
            switchState(GAMEPLAY);
        }
        print(newClient.isConnected);
    }

    void MostrarGameplay()
    {
        // Dependiendo del estado actual se van a desactivar el gui actual
        switch (currentState)
        {
            case MAIN_MENU:
                mainMenuPanel.gameObject.SetActive(false);
                break;
            case GAME_OVER:
                gameOverGUI.gameObject.SetActive(false);
                break;
            case ABANDONAR:
                abandonarPanel.gameObject.SetActive(false);
                break;
        }

        // Activar el gui a mostrar e inicializar
        gameplayHUD.gameObject.SetActive(true);

        Transform objetoAbandonar = gameplayHUD.Find("Abandonar");
        if (objetoAbandonar == null)
        {
            print("Error Abandonar no encontrado");
            return;
        }
        Button botonAbandonar = objetoAbandonar.GetComponent<Button>();
        botonAbandonar.onClick.RemoveAllListeners();
        botonAbandonar.onClick.AddListener(delegate { switchState(ABANDONAR); });
    }

    void MostrarGameOver()
    {
        // Dependiendo del estado actual se van a desactivar el gui actual
        switch (currentState)
        {
            case GAMEPLAY:
                break;
        }

        // Activar el gui a mostrar e inicializar

    }

    void MostrarAbandonar()
    {
        // Dependiendo del estado actual se van a desactivar el gui actual
        switch (currentState)
        {
            case GAMEPLAY:
                break;
        }

        // Activar el gui a mostrar e inicializar
        abandonarPanel.gameObject.SetActive(true);

        Transform objetoAbandonarSi = abandonarPanel.Find("AbandonarSi");
        if (objetoAbandonarSi == null)
        {
            print("Error AbandonarSi no encontrado");
            return;
        }
        Button botonAbandonarSi = objetoAbandonarSi.GetComponent<Button>();
        botonAbandonarSi.onClick.RemoveAllListeners();
        botonAbandonarSi.onClick.AddListener(AbandonarPartida);

        Transform objetoAbandonarNo = abandonarPanel.Find("AbandonarNo");
        if (objetoAbandonarNo == null)
        {
            print("Error AbandonarNo no encontrado");
            return;
        }
        Button botonAbandonarNo = objetoAbandonarNo.GetComponent<Button>();
        botonAbandonarNo.onClick.RemoveAllListeners();
        botonAbandonarNo.onClick.AddListener(delegate { switchState(GAMEPLAY); });

    }

    void AbandonarPartida()
    {
        // Función para deterner el host y cambiar de estado a main menu
        NetworkManager.singleton.StopHost();
        switchState(MAIN_MENU);
    }
}
