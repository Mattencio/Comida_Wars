﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DanoProyectil : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Enemy")
            coll.gameObject.SendMessage("AplicaDano", 10);
        
    }	
}
