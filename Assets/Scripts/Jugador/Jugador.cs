﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//añade esto pal online  :v
using UnityEngine.Networking;

[NetworkSettings(channel = 1, sendInterval = 0.05f)]
//remplaza el monobehaviour por networkingbehaviour
public class Jugador : NetworkBehaviour
{

    //private GameObject myProyectil;
    private Transform miResortera;

    private Rigidbody2D miProyectilRigidBody;
    private SpringJoint2D miResorte;

    public float releaseTime = 0.15f;
    public float tiempoVida = 6f;

    [SyncVar]
    public int playerID = 0;

    public GameObject miProyectil;

    private Vector3 localizacion;

    private bool isPressed = false;
    private bool isPlayerLayerSet = false;

    public static bool estaManipulando = false;
    public float xPlayer2Position = 16f;            // #Aldo posición del jugador dos para posicionar la cámara al inicio

    bool alAire = false;

    // Use this for initialization
    public override void OnStartLocalPlayer()
    {
        miResortera = this.gameObject.transform.GetChild(0);
        miProyectil = this.gameObject.transform.GetChild(1).gameObject;

        miResorte = miProyectil.GetComponent<SpringJoint2D>();
        miProyectilRigidBody = miProyectil.GetComponent<Rigidbody2D>();

        PlayersManager.textoPlayer = Network.player.ipAddress;        

        if (isServer)
        {
            PlayersManager.incrementarID();
            playerID = PlayersManager.recibirID();
            CmdGuardarIP(Network.player.ipAddress, playerID);
        }
        else
        {
            CmdIncrementarID();
            StartCoroutine(esperarID());
        }

    }    

    void Start()
    {

        //public override void OnStartNOTLocalPlayer xdxDDxdDD
        if (!isLocalPlayer)
        {
            miResortera = this.gameObject.transform.GetChild(0);
            miProyectil = this.gameObject.transform.GetChild(1).gameObject;
            miProyectilRigidBody = miProyectil.GetComponent<Rigidbody2D>();
            miResorte = miProyectil.GetComponent<SpringJoint2D>();
            //myProyectilRigidBody.isKinematic = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!isPlayerLayerSet)
        {
            establecerLayer();            
        }

        //solo para el local player

        if (isLocalPlayer)
        {
            //cuando el proyectil se suelta, lo que sucede después no se maneja dentro de 'update', el resto del show se maneja desde el método 'soltar'
            //que también es llamado por EventosMouse
            //Debug.Log(Network.player.ipAddress);
            if (isPressed)
            {                
                //la posición del mouse se da en pixeles , la posición de unity se da en otras
                //coordenadas, hay que hacer una conversión
                //localizacion = Camera.main.ScreenToWorldPoint (Input.mousePosition);
                //esta corrutina se ejecuta cada cierto tiempo para evitar la saturación de datos en el server

                localizacion = GetWorldPositionOnPlane(Input.mousePosition, 0);


                StartCoroutine(UpdatePosition());
            }
        }

    }


    IEnumerator esperarID()
    {

        yield return new WaitForSeconds(0.5f);
        CmdAsignarIDServer(this.netId, PlayersManager.recibirID(), Network.player.ipAddress);
        yield return new WaitForSeconds(0.5f);
        CmdGuardarIP(Network.player.ipAddress, playerID);
    }

    private void establecerLayer()
    {
        if (playerID == 2)
        {
            gameObject.layer = 10;
            miProyectil.layer = 10;
            // #Aldo_begin implementación para cambiar la perspectiva del jugador dos

            if (isLocalPlayer)
            {
                //float x = Camera.main.transform.position.x;
                float y = Camera.main.transform.position.y;
                float z = Camera.main.transform.position.z;

                Camera.main.transform.position = new Vector3(xPlayer2Position, y, -z);
                Camera.main.transform.Rotate(0, 180, 0);
            }

            isPlayerLayerSet = true;
            // #Aldo_end implementación para cambiar la perspectiva del jugador dos
        }

        if (playerID == 1)
        {
            gameObject.layer = 9;
            miProyectil.layer = 9;

            isPlayerLayerSet = true;
        }
    }

    [Command]
    private void CmdGuardarIP(string _ip, int _id)
    {
        PlayersManager.arrayIPsave(_ip, _id);
    }

    [Command]
    private void CmdIncrementarID()
    {
        PlayersManager.incrementarID();
    }

    [Command]
    private void CmdAsignarIDServer(NetworkInstanceId _netId, int _id, string _ip)
    {
        if (this.netId == _netId)
        {
            if (PlayersManager.getArrayIPserver().Contains(_ip))
            {
                Debug.Log("si lo contiene");
                playerID = PlayersManager.getArrayIPserver().IndexOf(_ip) + 1;
            }
            else
            {
                Debug.Log("no lo contiene");
                playerID = _id;
            }
        }
    }

    //esta función unicamente se llama a través de 'EventosMouse'
    public void setPressed()
    {
        if (isLocalPlayer && !alAire)
        {
            isPressed = true;
            estaManipulando = true;
        }
        //myProyectilRigidBody.isKinematic = true;
    }

    public void Soltar()
    {
        //lo ponemos en falso para que deje de actualizar la posicion del proyectil en el server
        if (isLocalPlayer)
        {
            isPressed = false;
            estaManipulando = false;
            if (!alAire)
            {
                // La bola está ahora en el aire
                alAire = true;

                //se guarda la ultima posicion del proyectil
                localizacion = miProyectilRigidBody.position;
                //se manda la posicion del proyectil al server y el ID del jugador que está lanzando el proyectil, para que lance el proyectil correcto en los dos celulares
                CmdRelease(playerID, localizacion);
                //cuando el proyectil deja de ser kinematico, la gravedad vuelve a actuar sobre el. Esto se necesita para que la resortera pueda darle el impulso
                //y para que choque contra el enemigo
                miProyectilRigidBody.isKinematic = false;
                //los primeros 150 ms el lanzamiento del proyectil se maneja completamente por el celular, esto es para evitar que el proyectil simplemente aparezca de la nada 150 ms
                //después de que sea lanzado en el server
                StartCoroutine(soltarProyectil());
                //esta corrutina no destruye el proyectil, sino que establece su posicion en el punto de lanzamiento y pone todas sus velocidades en 0, dando la apariencia de que es un nuevo
                //proyectil
                StartCoroutine(DestruirYa());
            }
        }


    }

    //el primer frame en el que el proyectil es lanzado, sucede esto
    IEnumerator soltarProyectil()
    {
        //releaseTime es 150 ms de espera, cuando este tiempo pase, la resortera se desactiva, así el proyectil tendrá impulso en los primeros instantes, esto solo sucede en el celular
        //de quien lanzó el proyectil
        yield return new WaitForSeconds(releaseTime);
        miResorte.enabled = false;
    }

    //esta corrutina actualiza constantemente la posicion del proyectil mientras el jugador está manipulandolo para preparar el lanzamiento
    IEnumerator UpdatePosition()
    {
        CmdSendPosition(localizacion, playerID);
        yield return new WaitForSeconds(0.05f);
    }

    //esta función no corre inmediatamente de ser llamada, sino que espera el tiempo que equivale 'tiempoVida', que es de 6 segundos en tipo float
    IEnumerator DestruirYa()
    {
        yield return new WaitForSeconds(tiempoVida);

        //una vez que transcurre ese tiempo desde que este método fue llamado, se ejecuta la siguiente linea
        CmdReiniciarProyectil(playerID);
    }

    //siempre se pide el ID para saber sobre qué proyectil se ejecutará el código respectivo
    [Command]
    public void CmdReiniciarProyectil(int _playerID)
    {
        //llama la función que reestablece los parámetros del proyectil. Notese que esta función está actuando en el server para que se ejecute en todos los clientes
        RpcReiniciarProyectil(_playerID);
    }

    [ClientRpc]
    public void RpcReiniciarProyectil(int _playerID)
    {
        if (playerID == _playerID)
        {
            //el proyectil se coloca en el punto de lanzamiento de la resortera
            miProyectilRigidBody.position = miResortera.position;
            //la velocidad es un vector 2D, ya que el mundo es 2D, velocidad en Y y en X. ambas deben de ser 0 al inicio
            miProyectilRigidBody.velocity = new Vector2(0, 0);
            //el proyectil no debe de estar rondando
            miProyectilRigidBody.angularVelocity = 0;
            //volvemos a habilitar la resortera
            miResorte.enabled = true;
            //la bola no está al aire
            alAire = false;
        }
    }

    //todos los comandos se ejecutan unicamente en el server, sólo el server es capaz de ejecutar funciones de tipo 'ClientRpc'
    //que son funciones que se ejecutan en todos los clientes
    [Command]
    public void CmdSendPosition(Vector3 position, int _playerID)
    {
        RpcSetKinematic(_playerID, position);
    }

    //cuando sueltas el proyectil en tu celular, el server no sabe desde que punto en el mundo lo soltaste, por eso se le envía la última posicion guardada en tu celular
    //junto con tu id, para identificar el proyecil a lanzar
    [Command]
    public void CmdRelease(int _playerID, Vector3 position)
    {
        //el proyectil del server obtiene la misma posicion que tu ves en tu pantalla
        miProyectilRigidBody.position = position;
        //tan pronto se coloca en esa posicon, deja de ser kinemático para empezar a ser jalado por la resortera que ya existe en el server
        miProyectilRigidBody.isKinematic = false;
        //esta corrutina espera 150 ms para liberar el proyectil y mandar la informacion del lanzamiento a todos los clientes
        StartCoroutine(soltarProyectilServer(_playerID));

    }

    //esta corrutina lanza el proyectil unicamente en el server
    //recuerda que este script lo tienen todos los proyectiles existentes, los clientes y el server, pero no queremos que los clientes ejecuten el código del server
    //por lo tanto esta instruccion sólo puede ser llamada dentro de un método de tipo 'Command'. Lanzar el proyectil en el server significa lanzar el proyectil en el celular
    //de la persona que es host de la partida. por lo tanto él si puede ver el lanzamiento en tiempo real
    IEnumerator soltarProyectilServer(int _playerID)
    {
        //en estos 150 ms el proyectil está siendo jalado por la resortera, así obtiene su impulso inicial, después de eso la resortera se 'apaga'
        yield return new WaitForSeconds(releaseTime);
        miResorte.enabled = false;
        //una vez que la resortera ha sido lanzada en el server, lo primero que hace es guardar los parámetros del proyectil para pasarlos a todos los clientes y todos ejecuten
        //la física del resto de la trayectoria apartir de estos parámetros iniciales
        RpcReturnPosition(_playerID, miProyectilRigidBody.position, miProyectilRigidBody.velocity, miProyectilRigidBody.angularVelocity);
    }

    //cuando un jugador lanza su proyectil en su celular, realmente sí lo está lanzando en su celular durante los primeros 150 ms y al mismo tiempo lo está lanzando en el server. 
    //Para que ambos proyectiles
    //estén sincronizados, el server manda la posicion y la velocidad que tiene del proyectil para que sean la misma en ambos dispositivos. Por lo tanto, después de 150 ms en tu celular, 
    //después de lanzar el proyectil, los parámetros de lanzamiento han sido reestablecidos por informacion obtenida del server

    //esto lanza el proyectil dentro de todos los clientes
    [ClientRpc]
    public void RpcReturnPosition(int _playerID, Vector3 position, Vector2 _velocity, float angularSpeed)
    {
        //que proyectil está siendo lanzado?. playerID es la variable local, puede ser player 1 o player 2. _playerID es la variable que viene del server. Que es quien lanzó su proyectil
        //recuerda que este código se está ejecutando dentro de todos los proyectiles y no queremos lanzarlos todos
        if (playerID == _playerID)
        {
            //esta parte del código sólo se ejecuta cuando ya transcurrieron los 150 ms del server, por lo tanto lo primero que hacer es desactivar la resortera
            miResorte.enabled = false;
            //kinematic false para que la física vuelva a actuar sobre el
            miProyectilRigidBody.isKinematic = false;
            //se establece la posicion y la velocidad que tiene el server del proyectil 
            miProyectilRigidBody.position = position;
            miProyectilRigidBody.velocity = _velocity;
            miProyectilRigidBody.angularVelocity = angularSpeed;
            Debug.Log("1");

        }
    }

    [ClientRpc]
    public void RpcSetKinematic(int _playerID, Vector3 position)
    {
        //dentro de cualquiera de los dos celulares, los dos proyectiles presentes son clientes del server, cada uno con un identificador propio
        //ya que este código se ejecuta en ambos clientes, primero se tiene que reconocer a cual de los dos se le está aplicando la ubiación del proyectil mientras el jugador
        //lo manipula
        if (playerID == _playerID)
        {
            //se tiene que hacer kinemático para que la resortera no intenté jalar el proyectil
            miProyectilRigidBody.isKinematic = true;
            //la posicion del proyectil se actualiza directamente del server, la variable 'position' que viene del server es la última posicion que tuvo el server del proyectil
            //esto quiere decir que cuando un jugador manipula su proyectil para lanzarlo, realmente está enviando al server la posicion en la que lo quiere acomodar 
            //y el proyectil recibe esta posicion directamente del server. El jugador no establece esta posicion de manera directa
            miProyectilRigidBody.position = position;

        }
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);

    }

}
