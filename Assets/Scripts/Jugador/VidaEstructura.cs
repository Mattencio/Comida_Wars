﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaEstructura : MonoBehaviour {

	private int vida = 100;
	public int playerNumber;
	private float maxDeltaX;
	private float deltaX;

    private RectTransform barraVida;

    private void Update()
    {
        // #Aldo_Begin - Si no se ha asignado la barra de vida del jugador, buscarla dependiendo del personaje y playerID
        if (barraVida == null && NetworkManager_Custom.singleton.isNetworkActive)
        {
            if (NetworkManager_Custom.playerID == 1)
            {
                if (playerNumber == 1)
                {
                    GameObject barraVidaGameObject = GameObject.Find("Barra verde local");
                    if (barraVidaGameObject != null)
                    {
                        barraVida = barraVidaGameObject.GetComponent<RectTransform>();
                    }
                    
                }
                else
                {
                    GameObject barraVidaGameObject = GameObject.Find("Barra verde remoto");
                    if (barraVidaGameObject != null)
                    {
                        barraVida = barraVidaGameObject.GetComponent<RectTransform>();
                    }
                }
            }
            else
            {
                if (playerNumber == 1)
                {
                    GameObject barraVidaGameObject = GameObject.Find("Barra verde remoto");
                    if (barraVidaGameObject != null)
                    {
                        barraVida = barraVidaGameObject.GetComponent<RectTransform>();
                    }
                    
                }
                else
                {
                    GameObject barraVidaGameObject = GameObject.Find("Barra verde local");
                    if (barraVidaGameObject != null)
                    {
                        barraVida = barraVidaGameObject.GetComponent<RectTransform>();
                    }
                }
            }
        }
        // #Aldo_End
    }

    void AplicaDano(int dano) {


		if(maxDeltaX == 0) {
			maxDeltaX = barraVida.sizeDelta.x;

		}


		vida -= dano;

		if(vida <= 0) {
			vida = 0;
			Debug.Log("morido");
		}

		deltaX = maxDeltaX * vida / 100;
		barraVida.sizeDelta = new Vector2(deltaX, barraVida.sizeDelta.y);

	}
}
