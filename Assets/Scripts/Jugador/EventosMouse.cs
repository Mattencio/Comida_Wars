using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class EventosMouse : MonoBehaviour {

	private GameObject myParent;
	private Jugador ball;
	// Use this for initialization
	void Start ()
	{

		
	}
	
	//se llama cuando el objeto es tocado por el jugador
	void OnMouseDown ()
	{
			//esta parte aún necesita modificaciones, ya que puedes tocar el proyectil del otro jugador y eso hará que caiga dentro de tu juego unicamente


			//cuando tocas el proyectil, no sabe quien es su padre (ke sad  :,v ) Así que se le asigna directamente la referencia en run time del objeto que es directamente su padre
			myParent = this.transform.parent.gameObject;
			//cuando le asignas un padre, obtiene el scrip que debe de ejecutar cuando lo presionas y cuando lo sueltas. Ya que forzosamente tienes que oprimirlo para soltarlo
			//y la referencia se guarda por el resto del juego, la parte que es la asignación se hace desde aquí
			ball = myParent.GetComponent<Jugador> ();
			//ejecuta el método de su padre del script ball
			ball.setPressed ();		
		
		
	}


	void OnMouseUp () {

		//ejecuta el método de su padre del script ball
			ball.Soltar();

	}
}
